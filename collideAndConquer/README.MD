#Collide and Conquer#
###Collision detection game

*written using Java EE technologies*

Collide and Conquer is a top down platform game, in which a player (or more in the future) needs to destroy running demons.

###Technologies, models and patterns
To me it's the first time to write a project in Java EE. I want to earn experience in this part of the Java universe. For this I use the EE technologies and maybe some extra. I will also try to use the actor model, because a lot of running players can cause a lot of simultanous access to one and the same thing.

* JavaEE: EJB, CDI, JPA, JSF(?)
* JavaFX (desktop) /JSF (web version?)
* Actor model
* Factories
* Managers (e.g. manager for player positions)
* Collision Detection
* ...  
  
  
###Functionalities
At first the only thing this application will be able to do is detect collisions. The players colliding will both disappear.  
Afterwards the players will be able to attack one another if the opponent is in it's attacking range depending on the weapon the attacker is carrying. Finally, power-ups will temporarily enhance your possibilities like attacking range, attacking speed, infinite life and so on.

For the result: don't expect too much. My situation is like a lot of Java developers say: "I'm not a web designer, so I can't design.", so don't expect to see a graphical wonder...